<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

</head>
<body>

<section id="welcome">

    @if (Route::has('login'))

        <header>

            <ul class="nav justify-content-end">

                @auth
                    <li class="nav-item">
                        <a href="{{ url('/home') }}" class="nav-link">Home</a>
                    </li>
                @else
                    <li class="nav-item">
                        <a href="{{ route('login') }}" class="nav-link">Login</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a href="{{ route('register') }}" class="nav-link">Register</a>
                        </li>
                    @endif
                @endauth

            </ul>

        </header>

    @endif

    <div class="container">
        <div class="row">

            <div class="wrapper-blog-list mx-auto">

                @foreach($posts as $post)

                <div class="card text-center mx-auto">
                    <div class="card-header">
                        {{ $post->user->email }}
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">{{ $post->title }}</h5>
                        <p class="card-text">{{ $post->description }}</p>
                        {{--<a href="#" class="btn btn-primary">Переход куда-нибудь</a>--}}
                    </div>
                    <div class="card-footer text-muted">
                        {{ $post->created_at->diffForHumans() }}
                    </div>
                </div>

                @endforeach

            </div>

        </div>
    </div>

</section>

</body>
</html>
